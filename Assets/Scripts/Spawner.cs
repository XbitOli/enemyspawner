using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Enemy _template;
    [SerializeField] private Transform _points;
    [SerializeField] private int _countdown = 1;

    private Transform[] _spawnPoints;
    private Coroutine _spawnEnemies;
    private int _maxRange;
    private int _minRange = 0;

    private void Start()
    {
        _spawnPoints = new Transform[_points.childCount];
        for (int i = 0; i < _spawnPoints.Length; i++)
        {
            _spawnPoints[i] = _points.GetChild(i);
        }

        _maxRange = _spawnPoints.Length;

        _spawnEnemies = StartCoroutine(SpawnEnemy());
    }
    private IEnumerator SpawnEnemy()
    {
        while (true)
        {
            int rand = UnityEngine.Random.Range(_minRange, _maxRange);
            Instantiate(_template, _spawnPoints[rand].position, Quaternion.identity);
            yield return new WaitForSeconds(_countdown);
        }
    }
}
